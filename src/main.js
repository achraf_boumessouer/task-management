import Vue from 'vue'
import './plugins/fontawesome'
import App from './App.vue'
import './css/app.css'
import router from './router/router';
import store from './store/main';


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
