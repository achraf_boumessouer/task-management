import Vue from 'vue';
import Vuex from 'vuex';
import defaultBoard from '../default-board';
import {SaveStatePlugin,uuid} from '../utils'

Vue.use(Vuex);

const board= JSON.parse(localStorage.getItem('board')) || defaultBoard

export default new Vuex.Store({
  plugins:[SaveStatePlugin],

  state: {
      board
  },
  getters:{
    getTask(){
      return (id)=>{
         for(const column of board.columns){
           for(const task of column.tasks){
             if(task.id == id)
             return task
           }
         } 
      }
    },

  },
  mutations: {
    CREATE_TASK(state,{tasks,name}){
      tasks.push(
        {
          name,
          id:uuid(),
          description:''
        }
      )
    },
    CREATE_COLUMN(state,{name}){
      state.board.columns.push({
        name,
        tasks:[]
      })
    },
    UPDATE_TASK(state,{task,key,value}){
      task[key]=value
    },
    MOVE_TASK(state,{fromTasks,toTasks,fromTaskIndex,toTaskIndex}){
      
      const taskToMove=fromTasks.splice(fromTaskIndex,1)[0]
      toTasks.splice(toTaskIndex,0,taskToMove)
    },
    MOVE_COLUMN(state,{fromColumnIndex,toColumnIndex}){
      
      const columnsList=state.board.columns;
      const columnToMove=columnsList.splice(fromColumnIndex,1)[0]
      columnsList.splice(toColumnIndex,0,columnToMove)
    },
  }
});
